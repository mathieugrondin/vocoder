#ifndef SYSTEMEAUDIO_H_
#define SYSTEMEAUDIO_H_

#include <string>
#include <vector>
#include <iostream>

#include <FMOD/fmod.hpp>
#include <FMOD/fmod_errors.h>

using namespace std;
using namespace FMOD;

class SystemeAudio
{
public :

	//Constructeur par d�faut appell� par l'application.
	SystemeAudio();
	//Destructeur appell� par l'application.
	~SystemeAudio();

	//Charge un fichier de musique situ� � l'emplacement aFichierMusique.
	//
	//Entr�e :
	//	aFichierMusique :	Emplacement du fichier sonore par rapport �
	//						l'ex�cutable courant. (Ou par rapport � la 
	//						racine d'un volume syst�me.)
	void	ChargerMusique(const string& aFichierMusique);

	//Retourne le nom du fichier sonore pr�sentement charg�.
	//Notons que le nom du fichier n'est pas n�cessairement le titre
	//musical de la s�quence sonore qu'il contient.
	string	GetFichierMusique() const;

	//Retourne le titre musical du fichier sonore pr�sentement
	//charg�. Si le titre n'est pas d�fini, retourne
	//une valeur indiquant "Titre inconnu".
	string	GetTitreMusique();

	//Retourne le nom de l'artiste du fichier sonore pr�sentement
	//charg�. Si le nom de l'artiste n'est pas d�fini, retourne
	//une valeur indiquant "Artiste inconnu".
	string  GetArtisteMusique();

	//D�marre (ou red�marre) la lecture du fichier sonore charg�.
	void	Play();

	//Arr�te la lecture du fichier sonore charg�.
	void	Pause();

	//Stoppe la lecture courante et lib�re le canal audio. Pour refaire
	//jouer la musique, il faut appeller "ChargerMusique" de nouveau afin
	//de r�allouer le canal audio.
	void	Stop();

	//D�fini le volume courante normalis� entre 0 et 1 du syst�me audio.
	void	SetVolume(float aVolume);

	//Retourne le volume courant normalis� entre 0 et 1 du syst�me audio.
	float	GetVolume() const;

	//Retourne le nombre de canaux audio du fichier sonore charg�.
	int		GetNbCanaux();

	//Retourne le nombre d'octet par �chantillon pour le fichier sonore
	//charg�.
	int		GetNbOctetParEchantillon();

	//Retourne la fr�quence d'�chantillonage du fichier sonore charg�.
	float	GetFrequenceEchantillonnage();

	//Lis autant d'�chantillons qu'il peut en rentrer dans aTailleTamponOctet octets �
	// la position de lecture courante de la musique, moins 1.5 * GetFrequenceEchantillonage() echantillons.
	// La valeur retourn�e correspond au nombre d'�chantillons lus et stock�s dans aTampon.
	//
	// Entr�es :
	//		aTampon :	Tampon de m�moire dans lequel seront �crites les donn�es.
	//		aTailleTamponOctet :	Taille en octet du tampon de m�moire dans lequel �crire les valeurs
	//								d'�chantillons
	unsigned int GetEchantillons(void* aTampon, unsigned int aTailleTamponOctet);

protected :
};
#endif //SYSTEMEAUDIO_H_
