#include "SystemeAudio.h"

FMOD_RESULT result;
System *systeme;
Sound *sound;
FMOD_CREATESOUNDEXINFO *info;
Channel * channel;
string file_name;

SystemeAudio::SystemeAudio()
{
	result = System_Create(&systeme);
	if (result != FMOD_OK)
	{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		exit(-1);
	}
	result = systeme->init(100, FMOD_INIT_NORMAL, 0);	// Initialize FMOD.
	if (result != FMOD_OK)
	{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		exit(-1);
	}
}

string SystemeAudio::GetFichierMusique() const
{
	string name = file_name.substr(file_name.find_last_of("/")+1,file_name.length());
	return name;
}
            
void SystemeAudio::Play()
{

	result = systeme->playSound(FMOD_CHANNEL_FREE, sound, false, &channel);
	if(result != FMOD_OK)
	{
		cout << FMOD_ErrorString(result) << " play";
	}
}

void SystemeAudio::Pause()
{
	result = channel->setPaused(true);
	if(result != FMOD_OK)
	{
		cout << FMOD_ErrorString(result) << " Pause";
	}
}

void SystemeAudio::Stop()
{
	result =sound->release();

	if(result != FMOD_OK)
	{
		cout << FMOD_ErrorString(result) << " Stop";
	}
}

void SystemeAudio::SetVolume(float aVolume)
{
	channel->setVolume(aVolume);
}

float SystemeAudio::GetVolume() const
{
	float vol;
	sound->getDefaults(NULL,&vol,NULL,NULL);
	return vol;
}

int SystemeAudio::GetNbCanaux()
{
	return info->numchannels;
}

int SystemeAudio::GetNbOctetParEchantillon()
{
	int bit;
	sound->getFormat(NULL,NULL,NULL,&bit);
	return bit/8;
}

float SystemeAudio::GetFrequenceEchantillonnage()
{
	float freq;
	sound->getDefaults(&freq,NULL,NULL,NULL);
	return freq;
}

unsigned int SystemeAudio::GetEchantillons(void* aTampon, unsigned int aTailleTamponOctet)
{
	unsigned int sample;
	unsigned int pos;
	channel->getPosition(&pos,FMOD_TIMEUNIT_MS);
	//channel->setPosition(pos-(1.5 * GetFrequenceEchantillonnage()),FMOD_TIMEUNIT_PCM);
	sound->readData(aTampon, aTailleTamponOctet, &sample);

	return sample;
}

void SystemeAudio::ChargerMusique(const string& aFichierMusique)
{
	file_name = aFichierMusique;
	sound->release();
	result = systeme->createStream(aFichierMusique.c_str(), FMOD_DEFAULT, 0, &sound);
	Play();
	channel->setPaused(true);
	if(result != FMOD_OK)
	{
		cout << FMOD_ErrorString(result) << " Charge";
	}
}

string SystemeAudio::GetTitreMusique()
{
	string titre = "titre inconnu";
	FMOD_TAG tag;
	sound->getTag("TITLE",NULL,&tag);
	if(tag.datatype == FMOD_TAGDATATYPE_STRING)
	{
		titre = (char *)tag.data;
	}
	return titre;
}

string SystemeAudio::GetArtisteMusique()
{
	string artist = "artist inconnu";
	FMOD_TAG tag;
	sound->getTag("ARTIST",NULL,&tag);
	if(tag.datatype == FMOD_TAGDATATYPE_STRING)
	{
		artist = (char *)tag.data;
	}
	return artist;
}

SystemeAudio::~SystemeAudio()
{
	sound->release();
	delete(this);
}