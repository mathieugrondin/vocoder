#pragma once

#include <cassert>
#include <iostream>
#include <stdlib.h>

typedef unsigned int uint;

class Interpolation
{
public:

	static double* linear(double* a_Input, uint a_InputSize, uint a_OutputSize);

private:

	static double* linearSimple(double* a_Input, double a_Ratio, uint a_InputSize, uint a_OutputSize);
};