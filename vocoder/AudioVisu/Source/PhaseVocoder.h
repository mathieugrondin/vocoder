#pragma once

#define _USE_MATH_DEFINES
#define REAL 0
#define IMAG 1

#include <cmath>
#include <iostream>
#include <math.h>

#include "FFTW/fftw3.h"

typedef unsigned int uint;
typedef int			 Semitones;
typedef double		 SampleMag;
typedef double		 Frequency;
typedef fftw_complex FreqBin;
typedef double		 Phase;
typedef double		 FreqMag;


class PhaseVocoder
{
public:

	PhaseVocoder(uint a_FrameSize, uint a_HopA, uint a_MaxNbTracks);
	~PhaseVocoder();

	void	    setSemitoneShift(Semitones a_SemitoneShift);
	void	    setSemitoneShifts(uint a_NbShifts, Semitones* a_SemitoneShifts);

	SampleMag** processFrame(SampleMag* a_NewFrameSamples);

private:

	uint		m_FrameSize;
	uint		m_HopA;
	uint*		m_HopS;
	uint		m_NbTracks;
	uint		m_MaxNbTracks;

	SampleMag*	m_FrameSamples;
	SampleMag** m_ShiftedSamples;
	SampleMag*	m_HannWindow;

	Frequency*	m_Frequencies;
	Phase*		m_PrevPhase;
	Phase**		m_CumulPhase;

	FreqBin*	m_FFTBuffer;
	FreqBin**	m_ShiftedSpectrums;
	fftw_plan	m_FFTWPlan;
	fftw_plan	m_IFFTWPlan;

	SampleMag*	generateHanningWindow(uint a_NbSamples);
	SampleMag*	applyWindowOn(SampleMag* m_Samples, uint a_Hop);
	FreqBin*	executeFFTOn(SampleMag* m_Samples);
	SampleMag*	executeIFFTOn(FreqBin* m_FreqBins);

	FreqMag		magnitudeOf(FreqBin a_FreqBin);
	Phase		phaseOf(FreqBin a_FreqBin);
	double		realPartOf(FreqMag a_FreqMag, Phase a_FreqPhase);
	double		imagPartOf(FreqMag a_FreqMag, Phase a_FreqPhase);
	double		wrapped(double a_Value);
};


inline FreqBin* PhaseVocoder::executeFFTOn(SampleMag* m_Samples)
{
	m_FrameSamples = m_Samples;
	fftw_execute(m_FFTWPlan);
	return m_FFTBuffer;
}

inline uint synthesisHopFor(uint a_HopA, Semitones a_SemitoneShift)
{
	return (uint)round(a_HopA * pow(2, ((double)a_SemitoneShift) / 12.0));
}

inline FreqMag PhaseVocoder::magnitudeOf(FreqBin a_FreqBin)
{
	return sqrt(a_FreqBin[REAL] * a_FreqBin[REAL] + a_FreqBin[IMAG] * a_FreqBin[IMAG]);
}

inline Phase PhaseVocoder::phaseOf(FreqBin a_FreqBin)
{
	return atan2(a_FreqBin[IMAG], a_FreqBin[REAL]);
}

inline double PhaseVocoder::realPartOf(FreqMag a_FreqMag, Phase a_FreqPhase)
{
	return a_FreqMag * cos(a_FreqPhase);
}

inline double PhaseVocoder::imagPartOf(FreqMag a_FreqMag, Phase a_FreqPhase)
{
	return a_FreqMag * sin(a_FreqPhase);
}

inline double PhaseVocoder::wrapped(double a_Value)
{
	return fmod(a_Value + M_PI, 2 * M_PI) - M_PI;
}