#include "Interpolation.h"

double* Interpolation::linear(double* a_Input, uint a_InputSize, uint a_OutputSize)
{
	double ratio = (double)a_OutputSize / (double)a_InputSize;

	if (ratio > 0.5 && ratio < 2)
	{
		return linearSimple(a_Input, ratio, a_InputSize, a_OutputSize);
	} 
	else 
	{
		std::cout << "Unimplemented interpolation algorithm." << std::endl;
	}

	return a_Input;
}

double* Interpolation::linearSimple(double* a_Input, double a_Ratio, uint a_InputSize, uint a_OutputSize)
{
	double* output = new double[a_OutputSize];
	double ratio = (double)a_InputSize / (double)a_OutputSize;
	double pos = 0;

	assert(ratio > 0.5);
	assert(ratio < 2);

	for (unsigned int i = 0; i < a_OutputSize; ++i)
	{
		uint xLeft = (uint)pos;
		uint xRight = xLeft + 1;
		double weight = pos - (double)xLeft;

		output[i] = (1.0 - weight) * a_Input[xLeft] + weight * a_Input[xRight];

		pos += ratio;
	}

	return output;
}