#include "PitchShifter.h"

PitchShifter::PitchShifter(uint   a_FrameSize,
	double a_FrameOverlap,
	uint   a_MaxNbTracks) :
	m_FrameSize(a_FrameSize),
	m_HopA((uint)round(((double)a_FrameSize) * (1.0 - a_FrameOverlap))),
	m_HopS(new uint[a_MaxNbTracks]),
	m_MaxNbTracks(a_MaxNbTracks),
	m_Vocoder(a_FrameSize, m_HopA, m_MaxNbTracks)
{}

PitchShifter::~PitchShifter()
{
	delete m_HopS;
}

SampleMag* PitchShifter::shiftPitch(const SampleMag* a_InputSignal,
	uint			 a_InputLength,
	Semitones		 a_SemitoneShift,
	uint			 a_SamplingRate)
{
	if (a_InputLength < m_FrameSize)
	{
		return nullptr;
	}
	m_InputSignal = a_InputSignal;
	m_InputLength = a_InputLength;
	m_SamplingRate = a_SamplingRate;
	m_HopS[0] = synthesisHopFor(m_HopA, a_SemitoneShift);
	m_Vocoder.setSemitoneShift(a_SemitoneShift);

	createFrames();

	for (uint f = 0; f < m_NbFrames; ++f)
	{
		m_ShiftedFrames[f] = m_Vocoder.processFrame(m_Frames[f])[0];
	}

	uint overlappedLength;
	/* SampleMag* stretchInput = */ return overlapAddFrames(overlappedLength);
	//SampleMag* output = Interpolation::linear(stretchInput, overlappedLength, m_InputLength);

	//deleteFrames();
	//delete stretchInput;

	//return output;
}

void PitchShifter::createFrames()
{
	m_NbFrames = (uint)((m_InputLength - m_FrameSize) / m_HopA) + 1;
	m_Frames = new SampleMag*[m_NbFrames];

	uint pos = 0;

	for (uint f = 0; f < m_NbFrames; ++f)
	{
		m_Frames[f] = new SampleMag[m_FrameSize];
		memcpy(m_Frames[f], m_InputSignal + pos, m_FrameSize * sizeof(SampleMag));
		pos += m_HopA;
	}

	m_ShiftedFrames = new SampleMag*[m_NbFrames];
}

void PitchShifter::deleteFrames()
{
	for (uint f = 0; f < m_NbFrames; ++f)
	{
		delete m_Frames[f];
	}
	delete m_Frames;
}

SampleMag* PitchShifter::overlapAddFrames(uint& a_OverlappedLength)
{
	a_OverlappedLength = m_NbFrames * m_HopS[0] + m_FrameSize - m_HopS[0];
	SampleMag* output = new SampleMag[a_OverlappedLength];

	for (uint n = 0; n < a_OverlappedLength; ++n)
	{
		output[n] = 0.0;
	}
	uint pos = 0;

	for (uint f = 0; f < m_NbFrames; ++f)
	{
		for (uint n = 0; n < m_FrameSize; ++n)
		{
			output[pos + n] += m_Frames[f][n];
		}
		pos += m_HopS[0];
	}

	return output;
}