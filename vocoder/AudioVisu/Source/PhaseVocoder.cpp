#include "PhaseVocoder.h"

PhaseVocoder::PhaseVocoder(uint a_FrameSize, uint a_HopA, uint a_MaxNbTracks) :
	m_FrameSize			(a_FrameSize),
	m_HopA				(a_HopA),
	m_HopS				(new uint[a_MaxNbTracks]),
	m_NbTracks			(1),
	m_MaxNbTracks		(a_MaxNbTracks),
	m_FrameSamples		(new SampleMag[m_FrameSize]),
	m_ShiftedSamples	(new SampleMag*[m_MaxNbTracks]),
	m_HannWindow		(generateHanningWindow(m_FrameSize)),
	m_Frequencies		(new Frequency[m_FrameSize]),
	m_PrevPhase			(new Phase[m_FrameSize]),
	m_CumulPhase		(new Phase*[m_FrameSize]),
	m_FFTBuffer			((FreqBin*)fftw_malloc(sizeof(FreqBin) * m_FrameSize)),
	m_ShiftedSpectrums	(new FreqBin*[m_MaxNbTracks]),
	m_FFTWPlan			(fftw_plan_dft_r2c_1d(m_FrameSize, m_FrameSamples, m_FFTBuffer, FFTW_MEASURE)),
	m_IFFTWPlan			(fftw_plan_dft_c2r_1d(m_FrameSize, m_FFTBuffer, m_FrameSamples, FFTW_MEASURE))
{
	for (uint track = 0; track < m_MaxNbTracks; ++track)
	{
		m_ShiftedSamples[track]	  = new SampleMag[m_FrameSize];
		m_CumulPhase[track]		  = new Phase[m_FrameSize];
		m_ShiftedSpectrums[track] = ((FreqBin*)fftw_malloc(sizeof(FreqBin)* m_FrameSize));
	}

	Frequency freqStep = 2.0 * M_PI / (double)m_FrameSize;

	for (uint bin = 0; bin < m_FrameSize; ++bin)
	{
		m_Frequencies[bin] = (double)bin * freqStep;
		m_PrevPhase[bin] = 0.0;
	}

	Semitones semitoneShift[] = {0};
	setSemitoneShifts(1, semitoneShift);
}

PhaseVocoder::~PhaseVocoder()
{
	for (uint track = 0; track < m_MaxNbTracks; ++track)
	{
		//delete m_ShiftedSamples[track];
		delete m_CumulPhase[track];
		fftw_free(m_ShiftedSpectrums[track]);
	}
	delete m_HopS;
	delete m_ShiftedSamples;
	delete m_HannWindow;
	delete m_Frequencies;
	delete m_PrevPhase;
	delete m_CumulPhase;
	fftw_destroy_plan(m_FFTWPlan);
	fftw_destroy_plan(m_IFFTWPlan);
	//fftw_free(m_FFTBuffer);
	//delete m_FrameSamples;
}

SampleMag* PhaseVocoder::generateHanningWindow(uint a_NbSamples)
{
	SampleMag* hanningWindow = new SampleMag[a_NbSamples];
	double cosArg = 2 * M_PI / (2.0 * (double)a_NbSamples - 1.0);

	for (uint n = 0; n < a_NbSamples; ++n)
	{
		hanningWindow[n] = 0.5 * (1 - cos((2 * n + 1) * cosArg));
	}

	return hanningWindow;
}

SampleMag* PhaseVocoder::applyWindowOn(SampleMag* m_Samples, uint a_Hop)
{
	double factor = 1.0 / sqrt((double)m_FrameSize / a_Hop * 0.5);

	for (uint n = 0; n < m_FrameSize; ++n)
	{
		m_Samples[n] *= m_HannWindow[n] * factor;
	}

	return m_Samples;
}

SampleMag* PhaseVocoder::executeIFFTOn(FreqBin* m_FreqBins)
{
	m_FFTBuffer = m_FreqBins;
	fftw_execute(m_IFFTWPlan);

	//double factor = 1.0 / (double)m_FrameSize;
	//for (uint n = 0; n < m_FrameSize; ++n)
	//{
	//	m_FrameSamples[n] *= factor;
	//}

	return m_FrameSamples;
}

void PhaseVocoder::setSemitoneShift(Semitones a_SemitoneShift)
{
	Semitones shift[] = { a_SemitoneShift };
	setSemitoneShifts(1, shift);
}

void PhaseVocoder::setSemitoneShifts(uint a_NbShifts, Semitones* a_SemitoneShifts)
{
	m_NbTracks = a_NbShifts;

	for (uint s = 0; s < m_NbTracks; ++s)
	{
		m_HopS[s] = synthesisHopFor(m_HopA, a_SemitoneShifts[s]);

		for (uint bin = 0; bin < m_FrameSize; ++bin)
		{
			m_CumulPhase[s][bin] = 0.0;
		}
	}
}

SampleMag** PhaseVocoder::processFrame(SampleMag* a_NewFrameSamples)
{
	m_FFTBuffer = executeFFTOn(applyWindowOn(a_NewFrameSamples, m_HopA));

	for (uint f = 0; f < m_FrameSize; ++f)
	{
		FreqMag   magnitude	 = magnitudeOf(m_FFTBuffer[f]);
		Phase	  phase		 = phaseOf(m_FFTBuffer[f]);
		Phase	  deltaPhase = phase - m_PrevPhase[f];
		m_PrevPhase[f]		 = phase;

		Frequency freqDevia  = deltaPhase - (double)m_HopA * m_Frequencies[f];
		Frequency trueFreq   = m_Frequencies[f] + wrapped(freqDevia) / (double)m_HopA;

		for (uint t = 0; t < m_NbTracks; ++t)
		{
			m_CumulPhase[t][f]			   = wrapped(m_CumulPhase[t][f] + (double)m_HopS[t] * trueFreq);
			m_ShiftedSpectrums[t][f][REAL] = realPartOf(magnitude, m_CumulPhase[t][f]);
			m_ShiftedSpectrums[t][f][IMAG] = imagPartOf(magnitude, m_CumulPhase[t][f]);
		}
	}

	for (uint t = 0; t < m_NbTracks; ++t)
	{
		m_ShiftedSamples[t] = applyWindowOn(executeIFFTOn(m_ShiftedSpectrums[t]), m_HopS[t]);
	}

	return m_ShiftedSamples;
}