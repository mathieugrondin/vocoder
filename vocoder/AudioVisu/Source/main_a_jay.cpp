#include <chrono>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

#include "PitchShifter.h"

const uint		FRAME_SIZE	   = 64;
const double	FRAME_OVERLAP  = 0.75;
const uint		INPUT_LENGTH   = 8192;
const uint		MAX_NB_TRACKS  = 1;
const Semitones	SEMITONE_SHIFT = 2;


SampleMag* generateCosCombinationSignal(SampleMag a_Amplitude, uint a_NbSamples, uint a_SamplingRate, Frequency* a_Frequencies, Phase* a_FreqPhases, uint a_nbFreq)
{
	SampleMag* cosWave = new SampleMag[a_NbSamples];

	double cosArg = 2.0 * M_PI / (double)a_SamplingRate;

	for (uint n = 0; n < a_NbSamples; ++n)
	{
		double cosSum = 0;
		for (uint f = 0; f < a_nbFreq; ++f)
		{
			cosSum += cos(cosArg * n * a_Frequencies[f] + a_FreqPhases[f]);
		}
		cosWave[n] = a_Amplitude * cosSum / (double) a_nbFreq;
	}

	return cosWave;
}

SampleMag* generateTestSignal()
{
	Frequency freq[] = { 50, 190 };
	Phase phases[] = { 0, 0.9 };
	return generateCosCombinationSignal(1, INPUT_LENGTH, 44100, freq, phases, 2);
}

void writeSignalToFile(SampleMag* a_Signal, uint a_SignalLength, const char* a_FileName)
{
	std::ofstream stream(a_FileName);
	for (uint n = 0; n < a_SignalLength; ++n)
	{
		stream << a_Signal[n] << std::endl;
	}
	stream.close();
}

int main()
{
	SampleMag* input = generateTestSignal();
	PitchShifter pitchShifter(FRAME_SIZE, FRAME_OVERLAP, MAX_NB_TRACKS);
	SampleMag* output =  pitchShifter.shiftPitch(input, INPUT_LENGTH, SEMITONE_SHIFT, 44100);

	writeSignalToFile(input, INPUT_LENGTH, "input.txt");
	writeSignalToFile(output, 9208, "output.txt");

	return 0;
}

int testInterpolation()
{
	SampleMag* input = generateTestSignal();
	uint outputLength = (uint)((double)INPUT_LENGTH * 0.6);
	SampleMag* output = Interpolation::linear(input, INPUT_LENGTH, outputLength);

	writeSignalToFile(input, INPUT_LENGTH, "input.txt");
	writeSignalToFile(output, outputLength, "output.txt");

	return 0;
}