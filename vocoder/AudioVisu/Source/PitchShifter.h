#pragma once

#define _USE_MATH_DEFINES

#include <cmath>
#include <math.h>

#include "Interpolation.h"
#include "PhaseVocoder.h"

class PitchShifter
{
public:

	PitchShifter(uint   a_FrameSize,
				 double a_FrameOverlap,
				 uint   a_MaxNbTracks);
	~PitchShifter();

	SampleMag* shiftPitch(const SampleMag* a_InputSignal,
						  uint			   a_InputLength,
						  Semitones		   a_SemitoneShift,
						  uint			   a_SamplingRate);

private:

	uint			 m_FrameSize;
	uint			 m_HopA;
	uint*			 m_HopS;
	uint			 m_MaxNbTracks;

	const SampleMag* m_InputSignal;
	uint			 m_InputLength;
	uint			 m_SamplingRate;

	SampleMag**		 m_Frames;
	SampleMag**		 m_ShiftedFrames;
	uint			 m_NbFrames;

	PhaseVocoder m_Vocoder;

	void createFrames();
	void deleteFrames();
	SampleMag* overlapAddFrames(uint& a_OverlappedLength);
};