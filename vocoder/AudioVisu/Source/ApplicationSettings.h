#ifndef APPLICATION_SETTINGS_H_
#define APPLICATION_SETTINGS_H_

#define WINDOW_POSX		300
#define WINDOW_POSY		300

#define WINDOW_WIDTH	960
#define WINDOW_HEIGHT	600

#define WINDOW_NAME		"Visualisateur de musique"

#define	FPS				60.0f
#define FRAME_DURATION	1000/FPS

#define TAILLE_TAMPON	131072
#define PLAYLIST		"resources/playlist.txt"


#endif //APPLICATION_SETTINGS_H_
